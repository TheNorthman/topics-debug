<?php defined('C5_EXECUTE') or die("Access Denied."); ?>


<form id="topic_test_form" method="post" action="<?php echo $this->action('save'); ?>">

	<h3>Blog Tags</h3>
	<?php $tags = CollectionAttributeKey::getByHandle('tags'); ?>
	<?php echo $tags->render('label'); ?>
	<?php echo $tags->render('form', false, true); ?>

	<h3>Core Project Topics</h3>
	<?php $project_topics = CollectionAttributeKey::getByHandle('project_topics'); ?>
	<?php echo $project_topics->render('label'); ?>
	<?php echo $project_topics->render('form', false, true); ?>
	<pre><?php print_r($project_topics); ?></pre>

	<h3>Our manual created topics</h3>
	<?php $topics_test = CollectionAttributeKey::getByHandle('topics_test'); ?>
	<?php echo $topics_test->render('label'); ?>
	<?php echo $topics_test->render('form', false, true); ?>
	<pre><?php print_r($topics_test); ?></pre>

</form>