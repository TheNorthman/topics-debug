<?php
namespace Concrete\Package\TopicsDebug;

use Package;
use SinglePage;

defined('C5_EXECUTE') or die("Access Denied.");

class Controller extends Package {

	protected $pkgHandle = 'topics_debug';
	protected $appVersionRequired = '5.7.4.2';
	protected $pkgVersion = '0.0.1';

	public function getPackageName() { return t("Topics Debug"); }
	public function getPackageDescription() { return t(""); }


	public function install() {
		$pkg = parent::install();

		// Our test page
		$singlePage = SinglePage::add('topics_test', $pkg);
		$singlePage->update(array('cName' => t('Topics Test'), 'cDescription' => t("")));

	}

}